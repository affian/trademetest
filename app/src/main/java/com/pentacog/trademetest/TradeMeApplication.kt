package com.pentacog.trademetest

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.pentacog.trademetest.dagger.AppModule
import com.pentacog.trademetest.dagger.DaggerRootAppComponent
import com.pentacog.trademetest.dagger.NetworkModule
import dagger.android.*
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject



class TradeMeApplication: Application(), HasActivityInjector, HasSupportFragmentInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingAndroidFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        val builder = DaggerRootAppComponent.builder()
        builder.appModule(AppModule(this))
                .networkModule(NetworkModule()).build().inject(this)

    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidFragmentInjector
    }

}