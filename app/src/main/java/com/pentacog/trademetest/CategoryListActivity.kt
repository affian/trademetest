package com.pentacog.trademetest

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.pentacog.trademetest.models.Category
import com.pentacog.trademetest.network.TradeMeRepository
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_category_list.*
import kotlinx.android.synthetic.main.category_list_content.view.*

import kotlinx.android.synthetic.main.category_list.*
import javax.inject.Inject

class CategoryListActivity : AppCompatActivity() {

    @Inject lateinit var tradeMeRepository: TradeMeRepository

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var mTwoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        setContentView(R.layout.activity_category_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (category_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true
        }

        tradeMeRepository.getCategory {
            val categories = it?.subcategories ?: ArrayList()
            setupRecyclerView(category_list, categories)
        }

    }

    private fun setupRecyclerView(recyclerView: RecyclerView, categories: List<Category>) {
        recyclerView.adapter = SimpleItemRecyclerViewAdapter(this, categories, mTwoPane)
    }

    class SimpleItemRecyclerViewAdapter(private val mParentActivity: CategoryListActivity,
                                        private val mValues: List<Category>,
                                        private val mTwoPane: Boolean) :
            RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val mOnClickListener: View.OnClickListener

        init {
            mOnClickListener = View.OnClickListener { v ->
                val item = v.tag as Category
                if (mTwoPane) {
                    val fragment = ListingStreamFragment.newInstance(item.number)
                    mParentActivity.supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.category_detail_container, fragment)
                            .commit()
                } else {
                    val intent = Intent(v.context, ListingStreamActivity::class.java).apply {
                        putExtra(ListingStreamFragment.ARG_CATEGORY, item.number)
                        putExtra(ListingStreamActivity.ARG_TITLE, item.name)
                    }
                    v.context.startActivity(intent)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.category_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = mValues[position]
            holder.mContentView.text = item.name

            with(holder.itemView) {
                tag = item
                setOnClickListener(mOnClickListener)
            }
        }

        override fun getItemCount(): Int {
            return mValues.size
        }

        inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
            val mContentView: TextView = mView.content
        }
    }
}
