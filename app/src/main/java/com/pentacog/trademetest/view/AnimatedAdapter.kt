package com.pentacog.trademetest.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.pentacog.trademetest.R

import java.util.ArrayList

abstract class AnimatedAdapter<T, VH : RecyclerView.ViewHolder>(context: Context) : RecyclerView.Adapter<VH>() {

    var objects: List<T> = ArrayList()
    var animation = R.anim.abc_slide_in_bottom

    private var mLastPosition = -1
    private val mContext: Context = context.applicationContext

    init {
        setHasStableIds(true)
    }

    constructor(context: Context, messages: List<T>) : this(context) {
        objects = messages
    }

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        val message = objects[position]
        onBindViewHolder(holder, position, message)

        if (holder.itemView != null) {
            applyAnimation(holder.itemView, position)
        }
    }

    override fun onViewDetachedFromWindow(holder: VH) {
        if (holder.itemView != null) {
            holder.itemView.clearAnimation()
        }
    }

    abstract fun onBindViewHolder(holder: VH, position: Int, obj: T)

    /**
     * Adds an animation to the cards loaded into the RecyclerView so
     * that new views animate in from the bottom as you scroll.
     * @param viewToAnimate The View to set the animation on
     * @param position The index of the View in the RecyclerView
     */
    private fun applyAnimation(viewToAnimate: View, position: Int) {
        if (position > mLastPosition && animation > 0) {
            val animation = AnimationUtils.loadAnimation(mContext, this.animation)
            viewToAnimate.startAnimation(animation)
            mLastPosition = position
        }
    }

    /**
     * Uses a Message's hash code to get a unique ID for each item.
     * @param position Item's index in the RecyclerView
     * @return Message's hash code as a unique ID
     */
    override fun getItemId(position: Int): Long {
        return objects[position]?.hashCode()?.toLong() ?: 0
    }

    override fun getItemCount(): Int {
        return objects.size
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

}