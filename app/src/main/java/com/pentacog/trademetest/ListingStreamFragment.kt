package com.pentacog.trademetest

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.TextUtils
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.pentacog.trademetest.models.Listing
import com.pentacog.trademetest.network.TradeMeRepository
import com.pentacog.trademetest.view.AnimatedAdapter
import com.squareup.picasso.Picasso
import com.wang.avi.AVLoadingIndicatorView
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class ListingStreamFragment : Fragment() {

    @Inject lateinit var tradeMeRepository: TradeMeRepository

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: CardAdapter
    private var mShortAnimationDuration: Int = 0

    /**
     * Used by the StaggeredGridLayoutManager to set the number of
     * columns to display based on device rotation.
     * @return 1 column for portrait and 2 for landscape.
     */
    private val columnCount: Int
        get() {
            return when (resources.configuration.orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> 2
                else -> 1
            }
        }

    companion object {
        val ARG_CATEGORY = "com.pentacog.trademetest.ARG_CATEGORY"

        fun newInstance(categoryId: String): ListingStreamFragment {
            val fragment = ListingStreamFragment()
            val args = Bundle()
            args.putString(ARG_CATEGORY, categoryId)

            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        mShortAnimationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)

    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.activity_double_column, null)

        if (view != null) {
            val loadingLayout = view.findViewById<AVLoadingIndicatorView>(R.id.loading_layout)

            mRecyclerView = view.findViewById(R.id.stream_recycler_view)
            val gridLayoutManager = StaggeredGridLayoutManager(columnCount, StaggeredGridLayoutManager.VERTICAL)
            mRecyclerView.layoutManager = gridLayoutManager

            mAdapter = CardAdapter(context)

            tradeMeRepository.getCategoryListings(arguments.getString(ARG_CATEGORY)) {
                if (it != null) {
                    val listings = it.list
                    mAdapter.objects = listings
                    if (listings.isEmpty()) {
                        activity.runOnUiThread({
                            loadingLayout.hide()
                            crossfadeViews(mRecyclerView, loadingLayout)
                        })
                    } else {
                        activity.runOnUiThread {
                            mAdapter.notifyItemRangeInserted(0, listings.size - 1)
                            crossfadeViews(loadingLayout, mRecyclerView)
                        }
                    }
                }
            }

            mRecyclerView.adapter = mAdapter
        }

        return view
    }

    private fun crossfadeViews(disappearing: View, appearing: View) {
        if (appearing.visibility != View.VISIBLE) {
            appearing.alpha = 0f
            appearing.visibility = View.VISIBLE
            appearing.animate()
                    .alpha(1f)
                    .setDuration(mShortAnimationDuration.toLong())
                    .setListener(null)
        }

        if (disappearing.visibility != View.GONE) {
            disappearing.animate()
                    .alpha(0f)
                    .setDuration(mShortAnimationDuration.toLong())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            disappearing.visibility = View.GONE
                        }
                    })
        }
    }

    private inner class CardAdapter(context: Context) : AnimatedAdapter<Listing, CardViewHolder>(context) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
            val layoutInflater = LayoutInflater.from(context)
            val view = layoutInflater.inflate(R.layout.card_text, null)

            return CardViewHolder(view)
        }

        override fun onBindViewHolder(holder: CardViewHolder, position: Int, obj: Listing) {

            val relativeTime = DateUtils.getRelativeTimeSpanString(getTimeFromDateString(obj.startDate), System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS)

            holder.relativeTime.text = relativeTime
            holder.price.text = obj.priceDisplay
            holder.rootView.clearAnimation()

            holder.title.text = obj.title
            holder.subtitle.text = obj.subtitle

            if (!TextUtils.isEmpty(obj.pictureHref)) {
                holder.imageView.visibility = View.VISIBLE
                Picasso.with(context).load(obj.pictureHref).into(holder.imageView)
            } else {
                holder.imageView.visibility = View.GONE
            }
        }

        fun getTimeFromDateString(dateString: String): Long {
            val trimmedDate: String = dateString.subSequence(6, dateString.length - 2).toString()
            return trimmedDate.toLong()
        }

    }

    private inner class CardViewHolder internal constructor(internal var rootView: View) : RecyclerView.ViewHolder(rootView) {
        internal var imageView: ImageView = rootView.findViewById(R.id.media_image_view)
        internal var title: TextView = rootView.findViewById(R.id.title_text_view)
        internal var subtitle: TextView = rootView.findViewById(R.id.content_text_view)
        internal var relativeTime: TextView = rootView.findViewById(R.id.timeTextView)
        internal var price:TextView = rootView.findViewById(R.id.priceTextView)
        internal var button: Button = rootView.findViewById(R.id.button)
    }

}
