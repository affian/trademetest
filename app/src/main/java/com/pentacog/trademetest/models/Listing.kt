package com.pentacog.trademetest.models

data class Listing(val listingId: Int,
                   val title: String,
                   val subtitle: String,
                   val category: String,
                   val startPrice: Number,
                   val buyNowPrice: Number,
                   val priceDisplay: String,
                   val isHighlighted: Boolean,
                   val maxBidAmount: Number,
                   val pictureHref: String?,
                   val startDate: String,
                   val photoUrls: List<String>)