package com.pentacog.trademetest.models


data class Category @JvmOverloads constructor(
        val name: String,
        val number: String,
        val path: String,
        val hasClassifieds: Boolean,
        val subcategories: List<Category> = ArrayList())