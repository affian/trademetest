package com.pentacog.trademetest.models

data class SearchResults(val totalCount: Int,
                    val page: Int,
                    val pageSize: Int,
                    val list: List<Listing>)