package com.pentacog.trademetest.dagger

import com.pentacog.trademetest.TradeMeApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules= arrayOf(AndroidSupportInjectionModule::class, AndroidInjectionModule::class, AppModule::class, ActivityModule::class, NetworkModule::class))
interface RootAppComponent : AndroidInjector<TradeMeApplication>