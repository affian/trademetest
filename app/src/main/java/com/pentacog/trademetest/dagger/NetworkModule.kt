package com.pentacog.trademetest.dagger

import dagger.Module
import android.app.Application
import javax.inject.Singleton
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.Gson
import com.pentacog.trademetest.BuildConfig
import com.pentacog.trademetest.network.TradeMeRepository
import com.pentacog.trademetest.network.PlaintextAuthenticationInterceptor
import com.pentacog.trademetest.network.TradeMeService
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpCache(application: Application): Cache {
        val cacheSize: Long = 10 * 1024 * 1024 // 10 MiB
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache, interceptors: java.util.Set<Interceptor>): OkHttpClient { //Java Set must be used because that's what Dagger builds
        val builder = OkHttpClient.Builder()
        builder.cache(cache)
        interceptors.forEach{builder.addInterceptor(it)}
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(TradeMeService.TM_ENDPOINT)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun provideTradeMeService(retrofit: Retrofit): TradeMeService {
        return retrofit.create(TradeMeService::class.java)
    }

    @Provides
    @IntoSet
    fun provideAuthInterceptor(): Interceptor {
        return PlaintextAuthenticationInterceptor(BuildConfig.TRADEME_CONSUMER_KEY, BuildConfig.TRADEME_CONSUMER_SECRET)
    }

    @Provides
    @Singleton
    fun provideTradeMeRepository(service: TradeMeService): TradeMeRepository {
        return TradeMeRepository(service)
    }

}