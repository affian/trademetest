package com.pentacog.trademetest.dagger

import android.app.Application
import com.pentacog.trademetest.CategoryListActivity
import dagger.Module
import javax.inject.Singleton
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
class AppModule(private val application:Application) {

    @Provides
    @Singleton
    fun providesApplication(): Application {
        return application
    }

}