package com.pentacog.trademetest.dagger

import com.pentacog.trademetest.ListingStreamActivity
import com.pentacog.trademetest.CategoryListActivity
import com.pentacog.trademetest.ListingStreamFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeCategoryListActivity(): CategoryListActivity

    @ContributesAndroidInjector
    abstract fun contributeListingStreamFragment(): ListingStreamFragment

    @ContributesAndroidInjector
    abstract fun contributeCategoryDetailActivity(): ListingStreamActivity
}