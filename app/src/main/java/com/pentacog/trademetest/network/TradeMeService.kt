package com.pentacog.trademetest.network

import com.pentacog.trademetest.models.Category
import com.pentacog.trademetest.models.Listing
import com.pentacog.trademetest.models.SearchResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface TradeMeService {

    companion object {
        val TM_ENDPOINT = "https://api.tmsandbox.co.nz/v1/"
    }

    @GET("Categories/{number}.json")
    fun getCategory(@Path("number")number: String): Call<Category>

    @GET("Search/General.json")
    fun getSearchResults(@QueryMap options: Map<String, String>): Call<SearchResults>

    @GET("Listings/{id}.json")
    fun getListing(@Path("id")listingId: String): Call<Listing>

}