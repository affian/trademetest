package com.pentacog.trademetest.network

import com.pentacog.trademetest.models.Category
import com.pentacog.trademetest.models.Listing
import com.pentacog.trademetest.models.SearchResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TradeMeRepository(private var tradeMeService: TradeMeService) {

    fun getCategory(categoryId: String = "0", callback: (Category?) -> Unit)  {
        tradeMeService.getCategory(categoryId).enqueue(CallbackWrapper(callback))
    }

    fun getListingDetail(listingId: String, callback: (Listing?) -> Unit) {
        tradeMeService.getListing(listingId).enqueue(CallbackWrapper(callback))
    }

    fun searchQuery(params: Map<String, String>, callback: (SearchResults?) -> Unit) {
        val map = params.toMutableMap()
        map.put(SearchParam.ROWS, "20")
        tradeMeService.getSearchResults(map).enqueue(CallbackWrapper(callback))
    }

    fun getCategoryListings(categoryId: String, callback: (SearchResults?) -> Unit) {
        val map = mapOf(SearchParam.CATEGORY to categoryId)
        searchQuery(map, callback)
    }

    private inner class CallbackWrapper<T>(private val callback: (T?) -> Unit): Callback<T> {

        override fun onFailure(call: Call<T>, t: Throwable) {
            callback.invoke(null)
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            callback.invoke(response.body())
        }

    }

}