package com.pentacog.trademetest.network

class SearchParam {
    companion object {
        val CATEGORY = "category"
        val ROWS = "rows"
    }
}