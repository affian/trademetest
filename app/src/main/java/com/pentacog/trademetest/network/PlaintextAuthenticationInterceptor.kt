package com.pentacog.trademetest.network

import okhttp3.Interceptor
import okhttp3.Response

class PlaintextAuthenticationInterceptor(private val consumerKey: String, private val consumerSecret: String): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
                .header("Authorization", "OAuth oauth_consumer_key=\"$consumerKey\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"$consumerSecret&\"")

        return chain.proceed(builder.build())
    }

}