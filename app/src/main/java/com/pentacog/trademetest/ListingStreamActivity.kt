package com.pentacog.trademetest

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_category_detail.*

class ListingStreamActivity : AppCompatActivity() {

    companion object {
        val ARG_TITLE = "com.pentacog.trademetest.ARG_TITLE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_detail)
        setSupportActionBar(detail_toolbar)

        title = intent.getStringExtra(ARG_TITLE)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = ListingStreamFragment.newInstance(intent.getStringExtra(ListingStreamFragment.ARG_CATEGORY))

            supportFragmentManager.beginTransaction()
                    .add(R.id.category_detail_container, fragment)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, CategoryListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
}
